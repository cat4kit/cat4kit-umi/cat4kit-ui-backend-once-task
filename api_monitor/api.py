# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from datetime import datetime, timedelta

from django.http import JsonResponse
from drf_api_logger.models import APILogsModel
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def api_info(request):
    result_for_200 = APILogsModel.objects.filter(status_code=200)
    result_for_301 = APILogsModel.objects.filter(status_code=301)
    result_for_404 = APILogsModel.objects.filter(status_code=404)
    result_for_500 = APILogsModel.objects.filter(status_code=500)

    today = datetime.now()
    today_calls = APILogsModel.objects.filter(
        added_on__year=today.year,
        added_on__month=today.month,
        added_on__day=today.day,
    )
    yesterday = today - timedelta(days=1)
    yesterday_calls = APILogsModel.objects.filter(
        added_on__year=yesterday.year,
        added_on__month=yesterday.month,
        added_on__day=yesterday.day,
    )
    two_days_ago = today - timedelta(days=2)
    two_days_ago_calls = APILogsModel.objects.filter(
        added_on__year=two_days_ago.year,
        added_on__month=two_days_ago.month,
        added_on__day=two_days_ago.day,
    )
    three_days_ago = today - timedelta(days=3)
    three_days_ago_calls = APILogsModel.objects.filter(
        added_on__year=three_days_ago.year,
        added_on__month=three_days_ago.month,
        added_on__day=three_days_ago.day,
    )
    four_days_ago = today - timedelta(days=4)
    four_days_ago_calls = APILogsModel.objects.filter(
        added_on__year=four_days_ago.year,
        added_on__month=four_days_ago.month,
        added_on__day=four_days_ago.day,
    )
    return JsonResponse(
        {
            "APICall_x": [
                four_days_ago.strftime("%Y-%m-%d"),
                three_days_ago.strftime("%Y-%m-%d"),
                two_days_ago.strftime("%Y-%m-%d"),
                yesterday.strftime("%Y-%m-%d"),
                today.strftime("%Y-%m-%d"),
            ],
            "APICall_y": [
                four_days_ago_calls.count(),
                three_days_ago_calls.count(),
                two_days_ago_calls.count(),
                yesterday_calls.count(),
                today_calls.count(),
            ],
            "status_codes": ["HTTP 200", "HTTP 301", "HTTP 404", "HTTP 500"],
            "status_codes_num": [
                result_for_200.count(),
                result_for_301.count(),
                result_for_404.count(),
                result_for_500.count(),
            ],
        }
    )
