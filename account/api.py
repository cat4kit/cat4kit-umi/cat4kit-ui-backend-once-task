from typing import Any, Dict, List

from authlib.integrations.django_client import OAuth
from django.conf import settings

# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_str
from django.utils.functional import cached_property
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django_helmholtz_aai.views import HelmholtzAuthentificationView
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.response import Response
from rest_framework.views import APIView

from .forms import ProfileForm, SignupForm
from .serializers import UserSerializer
from .tokens import account_activation_token


@api_view(["GET"])
def me(request):
    print(request.user)
    return JsonResponse(
        {
            "id": request.user.id,
            "email": request.user.email,
            "username": request.user.username,
        }
    )


def activate(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except Exception:
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()

    return redirect(settings.FRONTEND_URL)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def signup(request):
    data = request.data
    print(
        {
            "email": data.get("email"),
            "username": data.get("name"),
            "password1": data.get("password1"),
            "password2": data.get("password2"),
        }
    )
    message = "success"
    form = SignupForm(
        {
            "email": data.get("email"),
            "username": data.get("name"),
            "password1": data.get("password1"),
            "password2": data.get("password2"),
        }
    )
    print(form.errors.as_json())
    if form.is_valid():
        user = form.save(commit=False)
        user.is_active = False

        user.save()
        mail_subject = "Activate your user account."
        messagez = render_to_string(
            "template_activate_account.html",
            {
                "user": user.username,
                "domain": settings.WEBSITE_URL,
                "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                "token": account_activation_token.make_token(user),
                "protocol": "https" if request.is_secure() else "http",
            },
        )
        email = EmailMessage(mail_subject, messagez, to=[user.email])
        email.content_subtype = "html"  # Main content is now text/html
        email.send()

    else:
        message = form.errors.as_json()
    return JsonResponse({"message": message})


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def editprofile(request):
    data = request.data
    user = request.user
    if (
        User.objects.filter(email=data.get("email"))
        .exclude(pk=user.id)
        .exists()
    ):
        return JsonResponse({"message": "email already exists"})
    else:
        form = ProfileForm(request.POST, request.FILES, instance=user)
        if form.is_valid():
            form.save()
        serializer = UserSerializer(user)
        return JsonResponse(
            {
                "message": "Information updated successfully",
                "user": serializer.data,
            }
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def editpassword(request):
    user = request.user
    form = PasswordChangeForm(data=request.POST, user=user)
    print(form.errors.as_json())
    if form.is_valid():
        form.save()
        return JsonResponse({"message": "success"})
    else:
        return JsonResponse({"message": form.errors.as_json()}, safe=False)


class HelmholtzTokenObtainPairRefreshView(
    HelmholtzAuthentificationView, APIView
):
    oauth = OAuth()
    oauth.register(name="helmholtz", **settings.HELMHOLTZ_CLIENT_KWS)

    @cached_property
    def token(self) -> Dict[str, Any]:
        return self.oauth.helmholtz.authorize_access_token(self.request)

    @cached_property
    def userinfo(self) -> Dict[str, Any]:
        return self.oauth.helmholtz.userinfo(
            request=self.request, token=self.token
        )

    authentication_classes: List[str] = []
    permission_classes: List[str] = []

    def get(self, request, format=None) -> Response:
        """For listing out the posts, HTTP method: GET"""
        user_info = self.userinfo

        if user_info["email_verified"] is True:
            user_info["email_verified"] = "true"
        if user_info["email_verified"] is False:
            user_info["email_verified"] = "false"
        user_info["redirect_url"] = settings.LOGIN_REDIRECT_URL
        user_info["frontend_url"] = settings.FRONTEND_URL
        if (
            settings.HELMHOLTZ_ALLOWED_VOS_CAT4KIT[0]
            in user_info["eduperson_entitlement"]
        ):
            return render(
                request, "token_career.html", {**user_info, **self.token}
            )
        else:
            return redirect(settings.FRONTEND_URL)
