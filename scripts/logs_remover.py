# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0
import os
import sys
import traceback
from datetime import datetime, timedelta

import django

from logger.serializers import (  # noqa: E402 # isort:skip
    LoggerSchedulerSerializer,
    LoggerSerializer,
)
from tds2stac_ui.models import (  # noqa: E402 # isort:skip
    APIHarvester,
    STACValidator,
)

sys.path.append(
    os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
)  # set the directory we are working in (django manage.py directory)
os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", "cat4kit_ui_backend_once_task.settings"
)
django.setup()
from logger.models import Logger, LoggerScheduler  # noqa: E402 # isort:skip


def remove_logs():
    now = datetime.now()
    yesterday = now - timedelta(days=1)
    Logs = Logger.objects.filter(created_at__lte=yesterday).distinct("name")
    logs_serializers = LoggerSerializer(Logs, many=True)

    for i in logs_serializers.data:
        Harvested_details = APIHarvester.objects.filter(
            id=i["name"].split("_")[1]
        )
        Validator = STACValidator.objects.filter(
            harvester_id=i["name"].split("_")[1]
        )
        try:
            Logs_by_id = Logger.objects.filter(name=i["name"])
            Harvested_details.delete()
            Logs_by_id.delete()
            Validator.delete()

        except Exception:
            print(traceback.format_exc())
            continue


remove_logs()
# removing logs in the Scheduler


def remove_logs_scheduler():
    now = datetime.now()
    tw_days_ago = now - timedelta(days=2)
    Logs = LoggerScheduler.objects.filter(
        created_at__lte=tw_days_ago
    ).distinct("name")
    logs_serializers = LoggerSchedulerSerializer(Logs, many=True)

    for i in logs_serializers.data:
        try:
            Logs_by_id = LoggerScheduler.objects.filter(name=i["name"])
            Logs_by_id.delete()
        except Exception:
            print(traceback.format_exc())
            continue


remove_logs_scheduler()
