# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

import shutil

import pystac
from pystac import Catalog


def replace_all(all_files, data):
    root_catalog = Catalog.from_file(all_files + "/catalog.json")
    print(f"ID: {root_catalog.id}")
    print(f"Title: {root_catalog.title or 'N/A'}")
    print(f"Description: {root_catalog.description or 'N/A'}")
    print(f"Stac Links: {root_catalog.links}")
    print(list(root_catalog.get_children()))
    collections = list(root_catalog.get_collections())

    print(f"Number of collections: {len(collections)}")
    print("Collections IDs:")
    print(data.get("auto_collection_names"))

    for collection in collections:
        for i in range(len(data.get("auto_collection_names"))):
            if collection.id == data.get("auto_collection_names")[i]:
                if (
                    collection.description
                    != data.get("your_collection_descriptions")[i]
                    and not data.get("your_collection_descriptions")[i]
                    .strip()
                    .replace("\n", "\n\n")
                    .replace('"', "")
                    .isspace()
                    and data.get("your_collection_descriptions")[i]
                    .strip()
                    .replace("\n", "\n\n")
                    .replace('"', "")
                    != ""
                ):
                    collection.description = (
                        data.get("your_collection_descriptions")[i]
                        .strip()
                        .replace("\n", "\n\n")
                        .replace('"', "")
                        + "\n\n"
                        + collection.description
                    )

            if (
                collection.id == data.get("auto_collection_names")[i]
                and not data.get("your_collection_names")[i]
                .strip()
                .replace("\n", "\n\n")
                .replace('"', "")
                .isspace()
                and data.get("your_collection_names")[i]
                .strip()
                .replace("\n", "\n\n")
                .replace('"', "")
                != ""
            ):
                collection.id = (
                    data.get("your_collection_names")[i]
                    .strip()
                    .replace("\n", "\n\n")
                    .replace('"', "")
                )

                for collection_item in collection.get_items():
                    collection_item.collection_id = (
                        data.get("your_collection_names")[i]
                        .strip()
                        .replace("\n", "\n\n")
                        .replace('"', "")
                    )
                    continue
                shutil.rmtree(
                    all_files + "/" + data.get("auto_collection_names")[i]
                )

    root_catalog.normalize_hrefs(all_files)
    root_catalog.save(catalog_type=pystac.CatalogType.SELF_CONTAINED)
