# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from pystac_client import Client

catalog = Client.open("https://planetarycomputer.microsoft.com/api/stac/v1/")
print(len(catalog.get_child_links()))
print(catalog.get_items())
print(sum(1 for _ in catalog.get_items()))
