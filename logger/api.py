# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from datetime import datetime

import pytz
from django.http import JsonResponse
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)

from .forms import LoggerForm, LoggerSchedulerForm
from .models import Logger, LoggerScheduler
from .serializers import (
    LoggerNameSerializer,
    LoggerSchedulerSerializer,
    LoggerSerializer,
)


def querydict_to_dict(query_dict):
    data = {}
    for key in query_dict.keys():
        v = query_dict.getlist(key)
        if len(v) == 1:
            v = v[0]
        data[key] = v
    return data


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def logger_post(request):
    local_tz = pytz.timezone("Europe/Berlin")
    created_dt_formatted = datetime.fromtimestamp(
        float(request.data["created"])
    )
    created_dt_formatted = created_dt_formatted.astimezone(local_tz)
    # remember old state
    _mutable = request.data._mutable

    # set to mutable
    request.data._mutable = True

    # сhange the values you want
    request.data["created"] = created_dt_formatted.strftime(
        "%Y-%m-%dT%H:%M:%S.%fZ"
    )

    # set mutable flag back
    request.data._mutable = _mutable

    form = LoggerForm(request.data)
    print(form.is_valid(), form.errors)
    if form.is_valid():
        form.save()

    return JsonResponse("success", safe=False)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def logger_post_scheduler(request):
    local_tz = pytz.timezone("Europe/Berlin")
    created_dt_formatted = datetime.fromtimestamp(
        float(request.data["created"])
    )
    created_dt_formatted = created_dt_formatted.astimezone(local_tz)
    # remember old state
    _mutable = request.data._mutable

    # set to mutable
    request.data._mutable = True

    # сhange the values you want
    request.data["created"] = created_dt_formatted.strftime(
        "%Y-%m-%dT%H:%M:%S.%fZ"
    )

    # set mutable flag back
    request.data._mutable = _mutable

    form = LoggerSchedulerForm(request.data)
    print(form.is_valid(), form.errors)
    if form.is_valid():
        form.save()

    return JsonResponse("success", safe=False)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def logger_get(request, thing):
    Logs = Logger.objects.filter(name=thing)
    serializer = LoggerSerializer(Logs, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def logger_get_name(request):
    Logs = Logger.objects.distinct("name")

    serializer = LoggerNameSerializer(
        Logs[::-1][:3], context={"request": request}, many=True
    )

    return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def logger_get_scheduler(request):
    Logs = LoggerScheduler.objects.all()
    serializer = LoggerSchedulerSerializer(Logs, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def dashboard_basecard(request):
    # finished = 0
    harvested = 0
    # harvesting = 0
    ingested = 0
    # ingesting = 0
    harvested_scheduler = 0
    ingested_scheduler = 0

    job_length = len(Logger.objects.distinct())
    job_length_scheduler = len(LoggerScheduler.objects.distinct())

    Logs = Logger.objects.all()
    Logs_scheduler = LoggerScheduler.objects.all()
    serializer = LoggerSerializer(Logs, many=True)
    serializer_scheduler = LoggerSchedulerSerializer(Logs_scheduler, many=True)
    for i in range(len(serializer.data)):
        # if "Job finished successfully!" in serializer.data[i]['msg']:
        #     finished += 1
        if "Harvesting datasets is finished" in serializer.data[i]["msg"]:
            harvested += 1
        # if "Harvesting datasets is started" in serializer.data[i]['msg']:
        #     harvesting += 1
        if "Ingesting catalogs is finished" in serializer.data[i]["msg"]:
            ingested += 1
        # if "Ingesting catalogs is started" in serializer.data[i]['msg']:
        #     ingesting += 1
    for i in range(len(serializer_scheduler.data)):
        # if "Job finished successfully!" in serializer.data[i]['msg']:
        #     finished += 1
        if (
            "Harvesting datasets is finished"
            in serializer_scheduler.data[i]["msg"]
        ):
            harvested_scheduler += 1
        # if "Harvesting datasets is started" in serializer.data[i]['msg']:
        #     harvesting += 1
        if (
            "Ingesting catalogs is finished"
            in serializer_scheduler.data[i]["msg"]
        ):
            ingested_scheduler += 1
        # if "Ingesting catalogs is started" in serializer.data[i]['msg']:
        #     ingesting += 1

    return JsonResponse(
        {
            "jobs": job_length + job_length_scheduler,
            #  'finished' : finished,
            "harvested": harvested + harvested_scheduler,
            #  'harvesting' : harvesting,
            "ingested": ingested + ingested_scheduler,
            #  'ingesting' : ingesting,
        }
    )
