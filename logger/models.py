# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.db import models


class Logger(models.Model):
    levelname = models.CharField(max_length=500, blank=False, null=False)
    msg = models.CharField(max_length=500, blank=False, null=False)
    created_at = models.DateTimeField(
        auto_now_add=True, blank=False, null=False
    )
    created = models.CharField(
        max_length=500, blank=False, null=False
    )  # logger datetimes are stored as strings
    name = models.CharField(max_length=500, blank=False, null=False)


class LoggerScheduler(models.Model):
    levelname = models.CharField(max_length=500, blank=False, null=False)
    msg = models.CharField(max_length=500, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    created = models.CharField(
        max_length=500, blank=False, null=False
    )  # logger datetimes are stored as strings
    name = models.CharField(max_length=500, blank=False, null=False)
