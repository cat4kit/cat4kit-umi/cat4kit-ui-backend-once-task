# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.forms import ModelForm

from .models import Logger, LoggerScheduler


class LoggerForm(ModelForm):
    class Meta:
        model = Logger
        fields = ("levelname", "msg", "created", "name")


class LoggerSchedulerForm(ModelForm):
    class Meta:
        model = LoggerScheduler
        fields = ("levelname", "msg", "created", "name")
