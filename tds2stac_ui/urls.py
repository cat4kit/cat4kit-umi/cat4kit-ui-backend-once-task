# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.urls import path

from . import api

urlpatterns = [
    path(
        "tds2stac_ingesting/",
        api.harvesting_ingesting,
        name="tds2stac_ingesting",
    ),
    path(
        "catalog_editor_get/",
        api.catalog_editor_get,
        name="catalog_editor_get",
    ),
    path(
        "get_collection_names/<str:thing>/",
        api.get_collection_names,
        name="get_collection_names",
    ),
    path(
        "change_collection_names/<str:thing>/",
        api.change_collection_names,
        name="change_collection_names",
    ),
    path(
        "change_ingest_collection_names/<str:thing>/",
        api.change_ingest_collection_names,
        name="change_ingest_collection_names",
    ),
    path(
        "remove_collections/<str:thing>/",
        api.remove_collections,
        name="remove_collections",
    ),
    path(
        "remove_collections_failed/<str:thing>/",
        api.remove_collections_failed,
        name="remove_collections_failed",
    ),
]
