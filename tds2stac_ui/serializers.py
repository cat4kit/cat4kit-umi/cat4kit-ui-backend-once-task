# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
# SPDX-FileCopyrightText: 2023 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0

from logger.models import Logger
from rest_framework import serializers

from .models import APIHarvester, STACValidator


class HarvesterSerializer(serializers.ModelSerializer):
    class Meta:
        model = APIHarvester
        fields = (
            "id",
            "name",
            "email",
            "catalog",
            "stac",
            "stac_dir",
            "stac_id",
            "stac_desc",
            "service",
            "aggregated",
            "aggregated_url",
        )


class CatalagEditorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Logger
        fields = (
            "name",
            "created_at",
            "msg",
        )


class STACValidatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = STACValidator
        fields = (
            "harvester_id",
            "harvester_name",
            "catalog_valid",
            "catalog_msg",
            "collection_valid",
            "collection_msg",
        )
