# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.forms import ModelForm

from .models import APIHarvester, STACValidator


class HarvestingForm(ModelForm):
    class Meta:
        model = APIHarvester
        fields = (
            "name",
            "email",
            "catalog",
            "stac",
            "stac_dir",
            "stac_id",
            "stac_desc",
            "service",
            "aggregated",
            "aggregated_url",
        )


class STACValidatorForm(ModelForm):
    class Meta:
        model = STACValidator
        fields = (
            "harvester_id",
            "harvester_name",
            "catalog_valid",
            "catalog_msg",
            "collection_valid",
            "collection_msg",
        )
