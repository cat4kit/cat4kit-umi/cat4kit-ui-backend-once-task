# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
# SPDX-FileCopyrightText: 2023 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0

import os
import shutil
import traceback
from collections import OrderedDict

# from .serializers import APIHarvesterSerializer
from datetime import datetime

import pystac

# from django.conf import settings
from django.http import JsonResponse
from ds2stac_ingester import ds2stac_ingester
from logger.models import Logger
from logger.serializers import LoggerNameSerializer, LoggerSerializer
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from scripts.replace_collections import replace_all
from stac_check.lint import Linter
from tds2stac import app

from .forms import HarvestingForm, STACValidatorForm
from .models import APIHarvester, STACValidator
from .serializers import HarvesterSerializer, STACValidatorSerializer


def querydict_to_dict(query_dict):
    data = {}
    for key in query_dict.keys():
        v = query_dict.getlist(key)
        if len(v) == 1:
            v = v[0]
        data[key] = v
    return data


def merge(list1, list2, list3):
    merged_list = [
        (
            p1.strip().replace("\n", "\n\n").replace('"', ""),
            p2.strip().replace("\n", "\n\n").replace('"', ""),
            p3.strip().replace("\n", "\n\n").replace('"', ""),
        )
        for idx1, p1 in enumerate(list1)
        for idx2, p2 in enumerate(list2)
        for idx3, p3 in enumerate(list3)
        if idx1 == idx2
    ]
    return merged_list


def stac_validator(
    harvester_id, harvester_name, stac_dir, STACcatalog, STACcollction
):
    collection_valid = []
    catalog_valid = []
    catalog_msg = []
    collection_msg = []

    main_dir = stac_dir
    if STACcatalog:
        catalog = main_dir + "catalog.json"
        catalog_valid = [Linter(catalog).valid_stac]
        catalog_msg = Linter(catalog).best_practices_msg

    if STACcollction:
        for root, dirs, files in os.walk(main_dir):
            if root == main_dir:
                for dir in dirs:
                    collection = root + dir + "/collection.json"
                    collection_linter = Linter(collection)
                    collection_valid.append(collection_linter.valid_stac)
                    collection_msg.append(collection_linter.best_practices_msg)

    # if STACitems == True:
    #     for root, dirs, files in os.walk(main_dir):
    #         if files[0] != "catalog.json" and files[0] != "collection.json":
    #             item = root+"/"+files[0]
    #             item_linter = Linter(item)
    #             item_valid.append(item_linter.valid_stac)
    #             item_msg.append(item_linter.best_practices_msg)

    flat_list_catalog = list(
        dict.fromkeys(
            [item.strip() for item in catalog_msg if item.strip() != ""]
        )
    )

    flat_list_collections = list(
        dict.fromkeys(
            [
                item.strip()
                for sublist in collection_msg
                for item in sublist
                if item.strip() != ""
            ]
        )
    )

    # flat_list_items = list(dict.fromkeys([item.strip() for sublist in item_msg for item in sublist if item.strip() != '']))

    req = {
        "harvester_id": harvester_id,
        "harvester_name": harvester_name,
        "catalog_valid": catalog_valid,
        "catalog_msg": flat_list_catalog,
        "collection_valid": collection_valid,
        "collection_msg": flat_list_collections,
    }

    STACValidator.objects.update_or_create(
        harvester_id=harvester_id, defaults=req
    )
    # errors = form.errors.as_json()
    # if form.is_valid():
    #     saved_form = form.save()
    return JsonResponse({"message": "success validation"}, safe=False)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def harvesting_ingesting(request):
    if request.data["name"] == "" or request.data["name"].isspace():
        request.data["name"] = "job"
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit id"
    if request.data["stac_desc"] == "" or request.data["stac_desc"].isspace():
        request.data["stac_desc"] = "cat4kit description"
    request.data["stac_dir"] = "/app/stac"
    form = HarvestingForm(request.data)
    print("form.errors HarvestingForm", form.errors.as_json())
    if form.is_valid():
        saved_form = form.save()

    logger_name_get = (
        request.data["name"].strip().replace("\n", " ").replace("_", "-")
    )
    logger_id_get = saved_form.id
    if len(request.data["auto_collections_ids"]) != 0:
        merged_collection_tuples = merge(
            request.data["auto_collections_ids"],
            request.data["your_collections_ids"],
            request.data["your_collections_descriptions"],
        )
    else:
        merged_collection_tuples = None
    catalog_url_get = request.data["catalog"].strip().replace("\n", "")
    stac_get = request.data["stac"]
    stac_dir_get = (
        request.data["stac_dir"]
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )
    stac_id_get = request.data["stac_id"].strip().replace("\n", " ")
    stac_desc_get = request.data["stac_desc"].strip().replace("\n", " ")
    service_get = request.data["service"].lower()
    datetime_get = request.data["dateValue"]
    if len(datetime_get) != 0:
        start_datetime_format = datetime.strptime(
            datetime_get[0], "%Y-%m-%d %H:%M:%S"
        )
        end_datetime_format = datetime.strptime(
            datetime_get[1], "%Y-%m-%d %H:%M:%S"
        )
        datetime_string_get = [
            start_datetime_format.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            end_datetime_format.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        ]
    else:
        datetime_string_get = ["", ""]
    # ingestion_get = request.data['ingestion']
    if request.data["aggregated"] == "":
        aggregated_get = False
    else:
        aggregated_get = request.data["aggregated"]
    aggregated_url_get = (
        request.data["aggregated_url"].strip().replace("\n", " ")
    )
    # PRE STAC-Validation
    form1 = STACValidatorForm(
        {
            "harvester_id": saved_form.id,
            "harvester_name": request.data["name"],
            "catalog_valid": [],
            "catalog_msg": [],
            "collection_valid": [],
            "collection_msg": [],
        }
    )

    if form1.is_valid():
        form1.save()
    try:
        app.Harvester(
            catalog_url_get,
            logger_handler="HTTPHandler",
            logger_id=logger_id_get,
            logger_name=logger_name_get,
            logger_handler_host="127.0.0.1:8000",
            logger_handler_url="/api/logger/logger_post/",
            collection_tuples=merged_collection_tuples,
            stac=stac_get,
            stac_dir=stac_dir_get,
            stac_id=stac_id_get,
            stac_description=stac_desc_get,
            web_service=service_get,
            datetime_filter=datetime_string_get,
            # catalog_ingestion = False,
            # api_posting = False,
            aggregated_dataset=aggregated_get,
            aggregated_dataset_url=aggregated_url_get,
            limited_number=10,
        )
    except Exception:
        print(traceback.format_exc())
        current_validator = STACValidator.objects.get(
            harvester_id=saved_form.id
        )
        current_validator.delete()
        return JsonResponse(
            {
                "message": "failed in harvesting",
            },
            safe=False,
        )
    if request.data["catalog_ingestion"] != "No Ingestion":
        try:
            ds2stac_ingester.Ingester(
                stac_dir=stac_dir_get,
                logger_handler="HTTPHandler",
                logger_id=logger_id_get,
                logger_name=logger_name_get,
                logger_handler_host="127.0.0.1:8000",
                logger_handler_url="/api/logger/logger_post/",
                API_posting=False,
            )
        except Exception:
            print(traceback.format_exc())
            return JsonResponse(
                {
                    "message": "failed in ingesting",
                },
                safe=False,
            )
            current_harvester = APIHarvester.objects.get(id=saved_form.id)
            current_harvester.delete()
    if request.data["staccatalog"] or request.data["staccollecitons"]:
        try:
            stac_validator(
                saved_form.id,
                logger_name_get,
                stac_dir_get + "/stac/",
                request.data["staccatalog"],
                request.data["staccollecitons"],
            )
            return JsonResponse(
                {"message": "success validation"},
                safe=False,
            )
        except Exception:
            print(traceback.format_exc())
            return JsonResponse(
                {
                    "message": "failed in validity check",
                },
                safe=False,
            )

    return JsonResponse(
        {
            "message": "success",
            "log_detail": logger_name_get + "_" + str(logger_id_get),
        },
        safe=False,
    )


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def catalog_editor_get(request):
    finial_serializer = []
    Logs = Logger.objects.distinct("name")
    serializer = LoggerNameSerializer(Logs, many=True)

    logs_all = Logger.objects.all()
    serializer_all = LoggerSerializer(logs_all, many=True)
    validator_all = STACValidator.objects.all()
    # print(validator_all)
    validator_serializer = STACValidatorSerializer(validator_all, many=True)

    Harvester = APIHarvester.objects.all()
    harvester_serializer = HarvesterSerializer(Harvester, many=True)

    # print("(validator_serializer.data)",(validator_serializer.data))
    # TODO: Need to be refactored #21
    # This part should be removed in the deployment phase
    if len(serializer.data) != len(validator_serializer.data):
        for i in serializer.data:
            if i.get("name") not in [
                j.get("harvester_name") + "_" + j.get("harvester_id")
                for j in validator_serializer.data
            ]:
                validator_to_be_deleted = Logger.objects.filter(
                    name=i.get("name")
                )
                validator_to_be_deleted.delete()
        for i in validator_serializer.data:
            if i.get("harvester_name") + "_" + i.get("harvester_id") not in [
                j.get("name") for j in serializer.data
            ]:
                logs_to_be_deleted = STACValidator.objects.filter(
                    harvester_id=i.get("harvester_id")
                )
                logs_to_be_deleted.delete()

    for j in serializer.data:
        ordered_dict = OrderedDict()
        for k in validator_serializer.data:
            if (
                j.get("name")
                == k.get("harvester_name") + "_" + k.get("harvester_id")
                and k.get("catalog_valid") is not None
            ):
                ordered_dict["catalog_valid"] = k.get("catalog_valid")
                ordered_dict["collection_valid"] = {
                    "valid": (k.get("collection_valid")).count("True"),
                    "invalid": (k.get("collection_valid")).count("False"),
                }
                # ordered_dict["item_valid"] = {"valid": (k.get("item_valid")).count("True"),"invalid": (k.get("item_valid")).count("False")}
                ordered_dict["catalog_msg"] = k.get("catalog_msg")
                ordered_dict["collection_msg"] = k.get("collection_msg")
                # ordered_dict["item_msg"] = k.get("item_msg")
            elif (
                j.get("name")
                == k.get("harvester_name") + "_" + k.get("harvester_id")
                and k.get("catalog_valid") is None
            ):
                ordered_dict["catalog_valid"] = []
                ordered_dict["collection_valid"] = {}
                # ordered_dict["item_valid"] = {}
                ordered_dict["catalog_msg"] = []
                ordered_dict["collection_msg"] = []
                # ordered_dict["item_msg"] = []
        ordered_dict["harvesting"] = "running"
        ordered_dict["ingesting"] = "pending"
        ordered_dict["deleting"] = "pending"
        ordered_dict["name"] = j.get("name").split("_")[0]
        ordered_dict["id"] = j.get("name").split("_")[1]
        for i in serializer_all.data:
            if i.get("name") == j.get("name"):
                if "Harvesting datasets is finished" in i.get("msg"):
                    ordered_dict["harvesting"] = "finished"
                if (
                    "Activate services in the requested catalog or choose another service"
                    in i.get("msg")
                ):
                    ordered_dict["harvesting"] = "failed"
                if (
                    "Ingesting catalogs is started" in i.get("msg")
                    and ordered_dict["ingesting"] == "finished"
                ):
                    ordered_dict["ingesting"] = "finished"
                elif (
                    "Ingesting catalogs is started" in i.get("msg")
                    and ordered_dict["ingesting"] == "pending"
                ):
                    ordered_dict["ingesting"] = "running"
                if "Ingesting catalogs is finished" in i.get("msg"):
                    ordered_dict["ingesting"] = "finished"
                if "Ingesting catalogs is started" in i.get("msg"):
                    ordered_dict["ingesting"] = "finished"
                if "Deleting catalogs is started" in i.get("msg"):
                    ordered_dict["deleting"] = "running"
                if "Deleting catalogs is finished" in i.get("msg"):
                    ordered_dict["deleting"] = "finished"
        # print(ordered_dict)
        for m in harvester_serializer.data:
            if m.get("id") == ordered_dict.get("id"):
                ordered_dict["email"] = m.get("email")
        # for j in ordered_dict:
        #     print(j, ordered_dict.get(j))

        finial_serializer.append(ordered_dict)
        # print(finial_serializer)
    return JsonResponse(finial_serializer, safe=False)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def get_collection_names(request, thing):
    ordered_dict = OrderedDict()
    auto_collections = []
    your_collections_descriptions = []
    Harvested_details = APIHarvester.objects.filter(id=thing)
    serializer = HarvesterSerializer(Harvested_details, many=True)

    for root, dirs, files in os.walk(
        serializer.data[0].get("stac_dir")
        + "/"
        + serializer.data[0].get("name")
        + "_"
        + str(thing)
        + "/stac/"
    ):
        for file in files:
            if file == "collection.json":
                auto_collections.append(root.split("/")[-1])
                collection_file = pystac.Collection.from_file(
                    root + "/" + file
                )
                your_collections_descriptions.append(
                    collection_file.description
                )

    ordered_dict["auto_collections"] = auto_collections
    ordered_dict[
        "your_collections_descriptions"
    ] = your_collections_descriptions
    final_serializer = [ordered_dict]
    return JsonResponse(final_serializer, safe=False)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def change_collection_names(request, thing):
    Harvested_details = APIHarvester.objects.filter(id=thing)
    serializer = HarvesterSerializer(Harvested_details, many=True)
    # print(request.data)

    replace_all(
        serializer.data[0].get("stac_dir")
        + "/"
        + serializer.data[0].get("name")
        + "_"
        + str(thing)
        + "/stac/",
        request.data,
    )
    # print(all_files)
    return JsonResponse("success", safe=False)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def change_ingest_collection_names(request, thing):
    for i in range(len(request.data["your_collection_names"])):
        if (
            request.data["your_collection_names"][i]
            .strip()
            .replace("\n", "\n\n")
            .replace('"', "")
            != ""
        ):
            request.data["your_collection_names"][i] = (
                request.data["your_collection_names"][i]
                .strip()
                .replace("\n", "\n\n")
                .replace('"', "")
                + "_once_task_test"
            )
        else:
            request.data["your_collection_names"][i] = (
                request.data["auto_collection_names"][i] + "_once_task_test"
            )

    Harvested_details = APIHarvester.objects.filter(id=thing)
    serializer = HarvesterSerializer(Harvested_details, many=True)
    # print(request.data)

    replace_all(
        serializer.data[0].get("stac_dir")
        + "/"
        + serializer.data[0].get("name")
        + "_"
        + str(thing)
        + "/stac/",
        request.data,
    )
    ds2stac_ingester.Ingester(
        stac_dir=serializer.data[0].get("stac_dir")
        + "/"
        + serializer.data[0].get("name")
        + "_"
        + str(thing),
        logger_handler="HTTPHandler",
        logger_id=str(thing),
        logger_name=serializer.data[0].get("name"),
        logger_handler_host="127.0.0.1:8000",
        logger_handler_url="/api/logger/logger_post/",
        API_posting=False,
    )
    return JsonResponse("success", safe=False)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def remove_collections_failed(request, thing):
    Harvested_details = APIHarvester.objects.filter(id=thing)
    Logs = Logger.objects.filter(name__icontains=thing)
    Validator = STACValidator.objects.filter(harvester_id=thing)
    try:
        Harvested_details.delete()
        Logs.delete()
        Validator.delete()

        return JsonResponse("success", safe=False)
    except Exception:
        return JsonResponse("Already removed!", safe=False)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def remove_collections(request, thing):
    Harvested_details = APIHarvester.objects.filter(id=thing)
    Logs = Logger.objects.filter(name__icontains=thing)
    Validator = STACValidator.objects.filter(harvester_id=thing)

    serializer = HarvesterSerializer(Harvested_details, many=True)
    try:
        ds2stac_ingester.Deleter(
            stac_dir=serializer.data[0].get("stac_dir")
            + "/"
            + serializer.data[0].get("name")
            + "_"
            + str(thing),
            logger_handler="HTTPHandler",
            logger_id=str(thing),
            logger_name=serializer.data[0].get("name"),
            logger_handler_host="127.0.0.1:8000",
            logger_handler_url="/api/logger/logger_post/",
            API_posting=False,
        )
        Harvested_details.delete()
        Logs.delete()
        Validator.delete()
        shutil.rmtree(
            serializer.data[0].get("stac_dir")
            + "/"
            + serializer.data[0].get("name")
            + "_"
            + str(thing)
        )

        return JsonResponse("success", safe=False)
    except Exception:
        return JsonResponse("Already removed!", safe=False)
