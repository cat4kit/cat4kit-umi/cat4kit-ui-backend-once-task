# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

import uuid

from django.db import models


# Create your models here.
class APIHarvester(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=500, blank=False, null=False)
    email = models.EmailField(max_length=500, blank=False, null=False)
    catalog = models.CharField(max_length=500, blank=False, null=False)
    stac = models.BooleanField(default=False)
    stac_dir = models.CharField(max_length=500, blank=False, null=False)
    stac_id = models.CharField(max_length=500, blank=True, null=True)
    stac_desc = models.CharField(max_length=500, blank=True, null=True)
    ncml = "NCML"
    wms = "WMS"
    iso = "ISO"

    SERVICE_CHOICES = (
        (ncml, "ncml"),
        (wms, "wms"),
        (iso, "iso"),
    )
    service = models.CharField(
        max_length=500, choices=SERVICE_CHOICES, default=ncml
    )
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    ni = "No Ingestion"
    pgstac = "Ingestion directly to pgSTAC"
    post = "Ingestion via POST method to STAC-API"

    INGESTION_CHOICES = (
        (ni, "No Ingestion"),
        (pgstac, "Ingestion directly to pgSTAC"),
        (post, "Ingestion via POST method to STAC-API"),
    )
    ingestion = models.CharField(
        max_length=500, choices=INGESTION_CHOICES, default=ni
    )
    aggregated = models.BooleanField(default=False)
    aggregated_url = models.CharField(max_length=500, blank=True, null=True)


class STACValidator(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    harvester_id = models.CharField(max_length=500, blank=True, null=True)
    harvester_name = models.CharField(max_length=500, blank=True, null=True)
    catalog_valid = models.CharField(max_length=500, blank=True, null=True)
    catalog_msg = models.CharField(max_length=5000000, blank=True, null=True)
    collection_valid = models.CharField(max_length=500, blank=True, null=True)
    collection_msg = models.CharField(max_length=50000, blank=True, null=True)
    # item_valid = models.CharField(max_length=500, blank=True, null=True)
    # item_msg = models.CharField(max_length=5000000, blank=True, null=True)
