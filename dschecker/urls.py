# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.urls import path

from . import api

urlpatterns = [
    path("tds_checker/", api.tds_checker, name="tds_checker"),
    path(
        "get_collections_logger/<str:thing>/",
        api.get_collections_logger,
        name="get_collections_logger",
    ),
    path(
        "get_collections_ids/",
        api.get_collections_ids,
        name="get_collections_ids",
    ),
    path(
        "delete_collections_logger/<str:thing>/",
        api.delete_collections_logger,
        name="delete_collections_logger",
    ),
    path(
        "post_collections_logger/",
        api.post_collections_logger,
        name="post_collections_logger",
    ),
]
