# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

import traceback
from urllib.request import urlopen

from django.http import JsonResponse
from lxml import etree
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from tds2stac import app

from .forms import CatalogSummaryForm, LoggerForm
from .models import Logger
from .serializers import LoggerSerializer


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds_checker(request):
    service = []
    if request.data["catalog"].isspace() or request.data["catalog"] == "":
        return JsonResponse(
            {"TDS_status": "TDS Checker msg: No catalog URL provided."}
        )
    else:
        try:
            root = etree.parse(
                urlopen(
                    request.data["catalog"].replace(".html", ".xml"),
                    timeout=10,
                )
            ).getroot()
            for child in root:
                if "service" in str(child.tag):
                    for c in child:
                        if c.attrib["serviceType"] in ["WMS", "ISO", "NCML"]:
                            service.append(c.attrib["serviceType"])

            return JsonResponse(
                {
                    "TDS_status": "Thredds Data Server is accessible.",
                    "services": service,
                }
            )

        except Exception:
            print(traceback.format_exc())
            return JsonResponse(
                {
                    "TDS_status": "Checker msg: Thredds Data Server is not accessible. Contact TDS administrator."
                }
            )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def get_collections_ids(request):
    if request.data["catalog"].isspace() or request.data["catalog"] == "":
        return JsonResponse(
            {
                "message": "Collection's IDs finder msg: No catalog URL provided."
            }
        )
    else:
        try:
            etree.parse(
                urlopen(
                    request.data["catalog"]
                    .replace(".html", ".xml")
                    .strip()
                    .replace("\n", ""),
                    timeout=10,
                )
            ).getroot()
            form = CatalogSummaryForm(request.data)
            if form.is_valid():
                saved_form = form.save()

            if request.data["aggregated"] == "":
                aggregated_get = False
            else:
                aggregated_get = request.data["aggregated"]
            app.Catalog_summary(
                request.data["catalog"].strip().replace("\n", ""),
                logger_handler="HTTPHandler",
                logger_id=saved_form.id,
                logger_name="name",
                logger_handler_host="127.0.0.1:8000",
                logger_handler_url="/api/dschecker/post_collections_logger/",
                aggregated_dataset=aggregated_get,
            )
            return JsonResponse(
                {
                    "message": "successful",
                    "id": saved_form.id,
                }
            )

        except Exception:
            return JsonResponse(
                {
                    "message": "Collection ID msg: Unable to get the TDS to obtain the Collection IDs."
                }
            )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def post_collections_logger(request):
    form = LoggerForm(request.data)
    print(form.is_valid(), form.errors)
    if form.is_valid():
        form.save()
    return JsonResponse("success", safe=False)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def get_collections_logger(request, thing):
    Logs = Logger.objects.filter(name="name_" + thing)
    serializer1 = LoggerSerializer(Logs, many=True)
    for i in serializer1.data:
        for j in serializer1.data:
            if (i["msg"] != j["msg"]) and (i["msg"] in j["msg"]):
                Logger.objects.filter(msg=i["msg"]).delete()

    Logs1 = Logger.objects.filter(name="name_" + thing)
    serializer = LoggerSerializer(Logs1, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def delete_collections_logger(request, thing):
    Logs = Logger.objects.filter(name="name_" + thing)
    Logs.delete()
    return JsonResponse("success", safe=False)
