# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0
import uuid

from django.db import models


class TDSChecker(models.Model):
    name = models.CharField(max_length=500, blank=False, null=False)
    catalog = models.CharField(max_length=500, blank=False, null=False)


class Logger(models.Model):
    levelname = models.CharField(max_length=500, blank=False, null=False)
    msg = models.CharField(max_length=500, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    created = models.CharField(
        max_length=500, blank=False, null=False
    )  # logger datetimes are stored as strings
    name = models.CharField(max_length=500, blank=False, null=False)


class CatalogSummary(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    catalog = models.CharField(max_length=500, blank=False, null=False)
    aggregated = models.BooleanField(default=False)
