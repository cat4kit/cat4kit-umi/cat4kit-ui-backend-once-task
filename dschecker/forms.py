# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.forms import ModelForm

from .models import CatalogSummary, Logger


class LoggerForm(ModelForm):
    class Meta:
        model = Logger
        fields = ("levelname", "msg", "created", "name")


class CatalogSummaryForm(ModelForm):
    class Meta:
        model = CatalogSummary
        fields = (
            "catalog",
            "aggregated",
        )
