# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0
from rest_framework import serializers

from .models import CatalogSummary, Logger


class LoggerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Logger
        fields = ("msg",)


class CatalogSummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = CatalogSummary
        fields = (
            "id",
            "catalog",
            "aggregated",
        )
